package fr.seblor;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;

public class MC_1_10 extends Game {

	Screen scr = new MainScreen();
	FPSLogger _fpsLog;
	
	@Override
	public void create() {
		setScreen(scr);
		_fpsLog = new FPSLogger();
	}

	@Override
	public void render() {
		super.render();
		_fpsLog.log();
	}
}
