package fr.seblor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MainScreen implements Screen {

	public AssetManager manager;

	private SpriteBatch _batch;
	private Texture _water;
	private Texture _sun;
	private Sprite _spriteSun;
	private Player _fish;

	private BitmapFont _font;
	private String _text = "Utilisez les touches ZQSD ou les fl�ches directionnelles";

	private OrthographicCamera _camera;

	private int _waterWidth, _waterHeight, bufferX, bufferY;

	private int[] _sequence = { Keys.UP, Keys.UP, Keys.DOWN, Keys.DOWN, Keys.LEFT, Keys.RIGHT, Keys.LEFT, Keys.RIGHT,
			Keys.B, Keys.A };
	private int _currentKeyIndex = 0;

	@Override
	public void show() {
		_camera = new OrthographicCamera();
		_batch = new SpriteBatch();
		_water = new Texture(Gdx.files.internal("water.png"));
		_sun = new Texture(Gdx.files.internal("sun.png"));
		_spriteSun = new Sprite(_sun);
		_spriteSun.rotate(45);
		_fish = new Player(Gdx.files.internal("fish_clownfish_raw.png"), 0, 0, _waterWidth);

		manager = new AssetManager();
		manager.load("rick.mp3", Sound.class);

		_font = new BitmapFont();

	}

	public void renderBackground() {
		Gdx.gl.glClearColor(Color.valueOf("0099ff").r, Color.valueOf("0099ff").g, Color.valueOf("0099ff").b, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		_batch.setProjectionMatrix(_camera.combined);
		bufferX = 0;
		bufferY = _waterHeight;
		while (bufferY >= -_waterHeight) {
			while (bufferX < _waterWidth) {
				_batch.draw(_water, bufferX, bufferY, _camera.viewportWidth / 20, _camera.viewportWidth / 20);
				bufferX += _camera.viewportWidth / 20;
			}
			bufferY -= _camera.viewportWidth / 20;
			bufferX = 0;
		}
	}

	@Override
	public void render(float delta) {
		manager.update();

		queryInputs();

		_batch.begin();
		renderBackground();
		_font.draw(_batch, _text, _camera.viewportWidth - (_text.length() * _font.getXHeight()),
				_camera.viewportHeight - _font.getCapHeight());

		_fish.draw(_batch);
		_spriteSun.draw(_batch);
		_batch.end();
	}

	@Override
	public void resize(int width, int height) {
		this._waterWidth = width;
		this._waterHeight = height / 2;
		_camera.setToOrtho(false, width, height);
		_camera.position.set(width / 2, height / 2, 0);
		_camera.update();
		_spriteSun.setPosition(_camera.viewportWidth * 9 / 10, _camera.viewportHeight * 9 / 10);
		_spriteSun.setSize(_camera.viewportWidth / 20, _camera.viewportWidth / 20);

		_fish.setMaxWidth(_waterWidth);
		_fish.setX(_camera.viewportWidth / 2);
		_fish.setY(_camera.viewportHeight);
		_fish.setScale(_camera.viewportWidth / 250);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	private void queryInputs() {

		if (Gdx.input.isKeyJustPressed(_sequence[_currentKeyIndex])) {
			_currentKeyIndex++;
			System.out.println(_currentKeyIndex);
			if (_currentKeyIndex >= _sequence.length) {
				((Sound) manager.get("rick.mp3")).stop();
				((Sound) manager.get("rick.mp3")).loop();
				_currentKeyIndex = 0;
			}
		} else if (Gdx.input.isKeyJustPressed(Keys.ANY_KEY)) {
			_currentKeyIndex = 0;
		}

		boolean up = false, down = false, right = false, left = false;
		if (Gdx.input.isKeyPressed(Keys.Z) || Gdx.input.isKeyPressed(Keys.UP)) {
			up = true;
		}
		if (Gdx.input.isKeyPressed(Keys.Q) || Gdx.input.isKeyPressed(Keys.LEFT)) {
			left = true;
		}
		if (Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN)) {
			down = true;
		}
		if (Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT)) {
			right = true;
		}
		_fish.updatePosition(up, down, left, right, _fish.getY() < _waterHeight);

		if (Gdx.input.justTouched()) {
			if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
				_fish.setPosition(Gdx.input.getX(), _camera.viewportHeight - Gdx.input.getY());
				_fish.resetSpeed();
			}
			if (Gdx.input.isButtonPressed(Buttons.RIGHT)) {
				_text = "Un message pour le d�veloppeur ? seblor@seblor.fr ;)";
			}

		}
	}

}
