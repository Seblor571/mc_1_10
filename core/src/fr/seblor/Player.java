package fr.seblor;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Player extends Sprite {

	private float _vSpeed = 0;
	private float _hSpeed = 0;
	private int _maxWidth;

	public static final float acceleration = 0.5f;
	public static final float deceleration = 0.25f;
	public static final float maxSpeed = 10f;

	Player(FileHandle file, int x, int y, int maxWidth) {
		super(new Texture(file));
		_maxWidth = maxWidth;
	}

	public void setMaxWidth(int maxWidth) {
		_maxWidth = maxWidth;
	}

	public void setAngle(float x1, float y1, float x2, float y2) {

		if (x1 == x2 && y1 == y2)
			return;

		this.setRotation((float) Math.toDegrees(Math.atan2(y1 - y2, x1 - x2)) - 90 + 45);

		if (this.getRotation() > 45 || this.getRotation() < -135) {
			this.setFlip(true, false);
			this.setRotation(this.getRotation() - 90);
		} else {
			this.setFlip(false, false);
		}
	}

	public void updatePosition(boolean upKey, boolean downKey, boolean leftKey, boolean rightKey, boolean inWater) {

		float oldX = this.getX(), oldY = this.getY(), newX, newY;

		/*
		 * Up and Down
		 */
		if (inWater) {
			if (!(upKey ^ downKey)) { // Both or none
				if (_vSpeed > 0)
					_vSpeed -= Player.deceleration;
				else if (_vSpeed < 0)
					_vSpeed += Player.deceleration;
			} else if (upKey) {
				_vSpeed += Player.acceleration;
			} else if (downKey) {
				_vSpeed -= Player.acceleration;
			}

			if (_vSpeed > Player.maxSpeed)
				_vSpeed = Player.maxSpeed;
			else if (_vSpeed < -Player.maxSpeed)
				_vSpeed = -Player.maxSpeed;
		} else {
			_vSpeed -= Player.acceleration;
		}

		/*
		 * Left and Right
		 */
		if (!(leftKey ^ rightKey)) { // Both or none
			if (_hSpeed > 0)
				_hSpeed -= Player.deceleration;
			else if (_hSpeed < 0)
				_hSpeed += Player.deceleration;
		} else if (rightKey) {
			_hSpeed += Player.acceleration;
		} else if (leftKey) {
			_hSpeed -= Player.acceleration;
		}

		if (_hSpeed > Player.maxSpeed)
			_hSpeed = Player.maxSpeed;
		else if (_hSpeed < -Player.maxSpeed)
			_hSpeed = -Player.maxSpeed;

		/*
		 * Checking map bounds
		 */
		if (this.getY() + _vSpeed <= 0) {
			this.setY(0);
			_vSpeed = 0;
		}
		if (this.getX() + _hSpeed < 0) {
			this.setX(_maxWidth - this.getWidth());
		} else if (this.getX() + _hSpeed >= _maxWidth - this.getWidth()) {
			this.setX(0);
		}

		this.setPosition(this.getX() + _hSpeed, this.getY() + _vSpeed);

		newX = this.getX();
		newY = this.getY();
		setAngle(oldX, oldY, newX, newY);
		;
		// updateTexture();

	}

	public void resetSpeed() {
		_vSpeed = 0;
		_hSpeed = 0;
	}

}
