package fr.seblor.desktop;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import fr.seblor.MC_1_10;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.resizable = false;
		config.width = 640;
		config.height = 480;
		config.title = "Minecraft 1.10";
		config.allowSoftwareMode = true;
		config.addIcon("icon.png", FileType.Internal);
		new LwjglApplication(new MC_1_10(), config);
	}
}
